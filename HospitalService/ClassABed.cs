﻿//============================================================
// Student Number	: S10194816, S10195629
// Student Name	: Isaiah Low Jung, Glenn Peh Chia Kit
// Module  Group	: T01 
//============================================================



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HospitalService
{
    class ClassABed:Bed
    {
		private bool accompanyingPerson;

		public bool AccompanyingPerson
		{
			get { return accompanyingPerson; }
			set { accompanyingPerson = value; }
		}
		public ClassABed(int w, int b, double d, bool a) : base(w, b, d, a) { }
		public override double CalculateCharges(string cs, int d)
		{
			if (cs == "SC")
			{
				double TotalPrice = DailyRate * d;
				if (AccompanyingPerson == true)
				{
					return TotalPrice = TotalPrice + (100 * d);
				}
				return TotalPrice;
			}
			else if (cs == "PR")
			{
				double TotalPrice = DailyRate * d;
				if (AccompanyingPerson == true)
				{
					return TotalPrice = TotalPrice + (100 * d);
				}
				return TotalPrice;
			}
			else
			{
				double TotalPrice = DailyRate * d;
				if (AccompanyingPerson == true)
				{
					return TotalPrice = TotalPrice + (100 * d);
				}
				return TotalPrice;
			}
		}
		public override string ToString()
		{
			return base.ToString() + "\tAccompanying Person: " + AccompanyingPerson;
		}
	}
}
