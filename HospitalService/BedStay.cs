﻿//============================================================
// Student Number	: S10194816, S10195629
// Student Name	: Isaiah Low Jung, Glenn Peh Chia Kit
// Module  Group	: T01 
//============================================================



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HospitalService
{
    class BedStay
    {
		public DateTime StartBedstay { get; set; }

		public DateTime? EndBedstay { get; set; }

		public Bed Bed { get; set; }
		public BedStay (DateTime s, Bed b)
		{
			StartBedstay = s;
			Bed = b;
		}
		public override string ToString()
		{
			return "Start: " + StartBedstay +
				"\tBed: " + Bed;
		}
	}
}
