﻿//============================================================
// Student Number	: S10194816, S10195629
// Student Name	: Isaiah Low Jung, Glenn Peh Chia Kit
// Module  Group	: T01 
//============================================================



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HospitalService
{
    abstract class Bed
    {
		private int wardNo;

		public int WardNo
		{
			get { return wardNo; }
			set { wardNo = value; }
		}
		private int bedNo;

		public int BedNo
		{
			get { return bedNo; }
			set { bedNo = value; }
		}
		private double dailyRate;

		public double DailyRate
		{
			get { return dailyRate; }
			set { dailyRate = value; }
		}
		private bool available;

		public bool Available
		{
			get { return available; }
			set { available = value; }
		}
		public Bed (int w, int b, double d, bool a)
		{
			WardNo = w;
			BedNo = b;
			DailyRate = d;
			Available = a;
		}
		public abstract double CalculateCharges(string cs, int d);
		public override string ToString()
		{
			return "Ward No: " + WardNo +
				"\tBed No: " + BedNo +
				"\tDaily Rate: " + DailyRate + 
				"\tAvailability: " + Available; 
		}
	}
}
