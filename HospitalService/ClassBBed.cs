﻿//============================================================
// Student Number	: S10194816, S10195629
// Student Name	: Isaiah Low Jung, Glenn Peh Chia Kit
// Module  Group	: T01 
//============================================================



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace HospitalService
{
    class ClassBBed:Bed
    {
		private bool airCon;

		public bool AirCon
		{
			get { return airCon; }
			set { airCon = value; }
		}
		public ClassBBed(int w, int b, double d, bool a) : base(w, b, d, a) { }
		public override double CalculateCharges(string cs, int d)
		{
			if (cs == "SC")
			{
				double TotalPrice = (DailyRate * 0.3) * d;
				double Week = Math.Floor(Convert.ToDouble(d) / 7);
				if (AirCon == true)
				{
					if (d > 7)
					{
						return TotalPrice = TotalPrice + (Week * 100);
					}
					else
					{
						return TotalPrice = TotalPrice + (Week * 50);
					}
				}
				return TotalPrice;
			}
			else if (cs == "PR")
			{
				double TotalPrice = (DailyRate * 0.6) * d;
				double Week = Math.Floor(Convert.ToDouble(d) / 7);
				if (AirCon == true)
				{
					if (d > 7)
					{
						return TotalPrice = TotalPrice + (Week * 100);
					}
					else
					{
						return TotalPrice = TotalPrice + (Week * 50);
					}
				}
				return TotalPrice;
			}
			else
			{
				double TotalPrice = DailyRate * d;
				double Week = Math.Floor(Convert.ToDouble(d) / 7);
				if (AirCon == true)
				{
					if (d > 7)
					{
						return TotalPrice = TotalPrice + (Week * 100);
					}
					else
					{
						return TotalPrice = TotalPrice + (Week * 50);
					}
				}
				return TotalPrice;
			}
		}
		public override string ToString()
		{
			return base.ToString() + AirCon;
		}
	}
}
