﻿//============================================================
// Student Number	: S10194816, S10195629
// Student Name	: Isaiah Low Jung, Glenn Peh Chia Kit
// Module  Group	: T01 
//============================================================



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HospitalService
{
    class MedicalRecord
    {
        public string Diagnosis { get; set; }
        public double Temperature { get; set; }
        public DateTime DateTimeEntered { get; set; }
        public MedicalRecord (string d, double t, DateTime dte)
        {
            Diagnosis = d;
            Temperature = t;
            DateTimeEntered = dte;
        }
        public override string ToString()
        {
            return "Diagnosis: " + Diagnosis
                + "\tTemperature: " + Temperature
                + "\tDateTimeEntered: " + DateTimeEntered;
        }
    }
}
