﻿//============================================================
// Student Number	: S10194816, S10195629
// Student Name	: Isaiah Low Jung, Glenn Peh Chia Kit
// Module  Group	: T01 
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HospitalService
{
    class Stay
    {
        public DateTime AdmittedDate { get; set; }
        public DateTime? DischargeDate { get; set; }
        public bool IsPaid { get; set; }
        public List<MedicalRecord> MedicalRecordList { get; set; } = new List<MedicalRecord>();
        public List<BedStay> BedStayList { get; set; } = new List<BedStay>();
        public Patient Patient { get; set; }
        public Stay (DateTime ad, Patient p)
        {
            AdmittedDate = ad;
            Patient = p;
        }
        public void AddMedicalRecord(MedicalRecord mr)
        {
            MedicalRecordList.Add(mr);
        }
        public void AddBedstay(BedStay bs)
        {
            BedStayList.Add(bs);
        }
        public override string ToString()
        {
            return "Admitted Date: " + AdmittedDate +
                "\tPaid: " + IsPaid +
                "\tMedical Record List: " + MedicalRecordList +
                "\tBed Stay List: " + BedStayList +
                "\tPatient: " + Patient;
        }
    }
}
