﻿//============================================================
// Student Number	: S10194816, S10195629
// Student Name	: Isaiah Low Jung, Glenn Peh Chia Kit
// Module  Group	: T01 
//============================================================



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HospitalService
{
    public class ExRate
    {
        public Dictionary<string, double> Rates { get; set; }
        public string @base { get; set; }
        public string date { get; set; }
        public ExRate() { }
    }
}
