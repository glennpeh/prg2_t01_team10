﻿//============================================================
// Student Number	: S10194816, S10195629
// Student Name	: Isaiah Low Jung, Glenn Peh Chia Kit
// Module  Group	: T01 
//============================================================



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HospitalService
{
    class ClassCBed:Bed
    {
		public bool PortableTV { get; set; }
		public ClassCBed(int w, int b, double d, bool a) : base(w, b, d, a) { }
		public override double CalculateCharges(string cs, int d)
		{
			if (cs == "SC")
			{
				double TotalPrice = (DailyRate * 0.2) * d;
				if (PortableTV == true)
				{
					TotalPrice = TotalPrice + 30;
				}
				return TotalPrice;
			}
			else if (cs == "PR")
			{
				double TotalPrice = (DailyRate * 0.4) * d;
				if (PortableTV == true)
				{
					TotalPrice = TotalPrice + 30;
				}
				return TotalPrice;
			}
			else
			{
				double TotalPrice = DailyRate * d;
				if (PortableTV == true)
				{
					TotalPrice = TotalPrice + 30;
				}
				return TotalPrice;
			}
		}
		public override string ToString()
		{
			return base.ToString() + "\tPortable TV: " + PortableTV;
		}
	}
}
