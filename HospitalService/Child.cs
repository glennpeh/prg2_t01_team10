﻿//============================================================
// Student Number	: S10194816, S10195629
// Student Name	: Isaiah Low Jung, Glenn Peh Chia Kit
// Module  Group	: T01 
//============================================================



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HospitalService
{
    class Child:Patient
    {
        public double CdaBalance { get; set; }
        public Child(string i, string n, int a, char g, string cs, string s, double cdab): base(i, n, a, g , cs, s)
        {
            CdaBalance = cdab;
        }
        public override double CalculateCharges()
        {
            double final_charge = base.CalculateCharges();
            return final_charge;
        }
        public override string ToString()
        {
            return base.ToString() + "\tCDA Balance: " + CdaBalance;
        }
    }
}
