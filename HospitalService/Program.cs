﻿//=============================================================
// Student Number	: S10194816, S10195629
// Student Name	: Isaiah Low Jung, Glenn Peh Chia Kit
// Module  Group	: T01 
//=============================================================




using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Http;
using Newtonsoft.Json;


namespace HospitalService
{
    class Program
    { 
        static void Main(string[] args)
        {
            List<Patient> PatientList = new List<Patient>();
            List<Bed> BedList = new List<Bed>();
            InitData(PatientList, BedList);
            while (true)
            {
                Menu();
                Console.Write("Enter your Option: ");
                string option = Console.ReadLine();
                if (option == "1")
                {
                    Console.WriteLine("Option 1: View All Patients");
                    DisplayPatient(PatientList);
                }
                else if (option == "2")
                {
                    Console.WriteLine("Option 2: View All Beds");
                    DisplayBed(BedList);
                }
                else if (option == "3")
                {
                    Console.WriteLine("Option 3: Register Patient");
                    AddPatient(PatientList);
                }
                else if (option == "4")
                {
                    Console.WriteLine("Option 4: Add new Bed");
                    AddBed(BedList);
                }
                else if (option == "5")
                {
                    Console.WriteLine("Option 5: Register new Stay");
                    RegisterStay(PatientList, BedList);
                }
                else if (option == "6")
                {
                    Console.WriteLine("Option 6: Retrieve Patient Details");
                    PatientDetail(PatientList);
                }
                else if (option == "7")
                {
                    Console.WriteLine("Add Medical Record Entry");
                    AddMedicalRecord(PatientList);
                }
                else if (option == "8")
                {
                    Console.WriteLine("Option 8: View Medical Records");
                    ViewRecords(PatientList);
                }
                else if (option == "9")
                {
                    Console.WriteLine("Transfer Patient to Another Bed");
                    ChangeBed(PatientList, BedList);
                }
                else if (option == "10")
                {
                    Console.WriteLine("Option 10. Discharge and payment");
                    DischargePayment(PatientList, BedList);
                }
                else if (option == "11")
                {
                    Console.WriteLine("Option 11: Display Exchange Rate");
                    ExchangeRate();
                }
                else if (option == "12")
                {
                    Console.WriteLine("Option 12: Display PM 2.5 Information");
                    APIPM();
                }
                else if (option == "0")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Invalid Input");
                }
            }
        }
        static void Menu()
        {
            //method for printing out menu
            Console.WriteLine("MENU");
            Console.WriteLine("=====");
            Console.WriteLine("1. View All Patients");
            Console.WriteLine("2. View All Beds");
            Console.WriteLine("3. Register Patient");
            Console.WriteLine("4. Add New Bed");
            Console.WriteLine("5. Register a Hospital Stay");
            Console.WriteLine("6. Retrieve Patient Details");
            Console.WriteLine("7. Add Medical Record Entry");
            Console.WriteLine("8. View Medical Records");
            Console.WriteLine("9. Transfer Patient to Another Bed");
            Console.WriteLine("10. Discharge and Payment");
            Console.WriteLine("11. Display Currencies and Exchange Rate");
            Console.WriteLine("12. Display PM 2.5 information");
            Console.WriteLine("0. Exit");
            Console.WriteLine("");
        }
        static void InitData(List<Patient> pList, List<Bed> bList)
        {
            //method for loading csv files into lists

            //loading patients into patientlist
            string[] patientLines = File.ReadAllLines("Patients.csv");
            for (int i = 1; i < patientLines.Length; i++)
            {
                string[] data = patientLines[i].Split(',');
                if (Convert.ToInt32(data[2]) <= 12)
                {
                    if (data[4] == "SC")
                    {
                        Patient p = new Child(data[1], data[0], Convert.ToInt32(data[2]), Convert.ToChar(data[3]), data[4], "Registered", Convert.ToDouble(data[5]));
                        p.Stay = null;
                        pList.Add(p);
                    }
                    else
                    {
                        Patient p = new Child(data[1], data[0], Convert.ToInt32(data[2]), Convert.ToChar(data[3]), data[4], "Registered", 0);
                        p.Stay = null;
                        pList.Add(p);
                    }
                }
                else if (Convert.ToInt32(data[2]) < 65)
                {
                    if (data[4] == "SC" || data[4] == "PR")
                    {
                        Patient p = new Adult(data[1], data[0], Convert.ToInt32(data[2]), Convert.ToChar(data[3]), data[4], "Registered", Convert.ToDouble(data[5]));
                        p.Stay = null;
                        pList.Add(p);
                    }
                    else
                    {
                        Patient p = new Adult(data[1], data[0], Convert.ToInt32(data[2]), Convert.ToChar(data[3]), data[4], "Registered", 0);
                        p.Stay = null;
                        pList.Add(p);
                    }
                }
                else
                {
                    Patient p = new Senior(data[1], data[0], Convert.ToInt32(data[2]), Convert.ToChar(data[3]), data[4], "Registered");
                    p.Stay = null;
                    pList.Add(p);
                }
            }
            //loading beds into bedlist
            string[] bedLines = File.ReadAllLines("Beds.csv");
            for (int i = 1; i < bedLines.Length; i++)
            {
                string[] data = bedLines[i].Split(',');
                if (data[0] == "A")
                {
                    Bed b = new ClassABed(Convert.ToInt32(data[1]), Convert.ToInt32(data[2]), Convert.ToDouble(data[4]), BoolConverter(data[3]));
                    bList.Add(b);
                }
                else if (data[0] == "B")
                {
                    Bed b = new ClassBBed(Convert.ToInt32(data[1]), Convert.ToInt32(data[2]), Convert.ToDouble(data[4]), BoolConverter(data[3]));
                    bList.Add(b);
                }
                else
                {
                    Bed b = new ClassCBed(Convert.ToInt32(data[1]), Convert.ToInt32(data[2]), Convert.ToDouble(data[4]), BoolConverter(data[3]));
                    bList.Add(b);
                }
            }
        }
        static void DisplayPatient(List<Patient> pList)
        {
            //method for listing all patients
            Console.WriteLine("{0,-13}{1,-10}{2,-5}{3,-7}{4,-13}{5,-12}{6,-8}", "Name", "ID", "Age", "Gender", "Citizenship", "Status", "Balance");
            foreach (Patient p in pList)
            {
                if (p is Child)
                {
                    Child c = (Child)p;
                    Console.WriteLine("{0,-13}{1,-10}{2,-5}{3,-7}{4,-13}{5,-12}{6,-8}", c.Name, c.ID, c.Age, c.Gender, c.CitizenStatus, c.Status, c.CdaBalance);
                }
                else if (p is Adult)
                {
                    Adult a = (Adult)p;
                    Console.WriteLine("{0,-13}{1,-10}{2,-5}{3,-7}{4,-13}{5,-12}{6,-8}", a.Name, a.ID, a.Age, a.Gender, a.CitizenStatus, a.Status, a.MedisaveBalance);
                }
                else
                {
                    Senior s = (Senior)p;
                    Console.WriteLine("{0,-13}{1,-10}{2,-5}{3,-7}{4,-13}{5,-12}", s.Name, s.ID, s.Age, s.Gender, s.CitizenStatus, s.Status);
                }
            }
        }
        static void DisplayBed(List<Bed> bList)
        {
            //method for listing all beds
            Console.WriteLine("{0, -4}{1,-5}{2,-8}{3,-7}{4,-11}{5,-9}", "No.", "Type", "WardNo", "BedNo", "DailyRate", "Available");
            for (int i = 0; i < bList.Count; i++)
            {
                if (bList[i] is ClassABed)
                {
                    ClassABed a = (ClassABed)bList[i];
                    Console.WriteLine("{0,-4}{1,-5}{2,-8}{3,-7}{4,-11}{5,-9}", i + 1, "A", a.WardNo, a.BedNo, a.DailyRate, a.Available);
                }
                else if (bList[i] is ClassBBed)
                {
                    ClassBBed b = (ClassBBed)bList[i];
                    Console.WriteLine("{0,-4}{1,-5}{2,-8}{3,-7}{4,-11}{5,-9}", i + 1, "B", b.WardNo, b.BedNo, b.DailyRate, b.Available);
                }
                else
                {
                    ClassCBed c = (ClassCBed)bList[i];
                    Console.WriteLine("{0,-4}{1,-5}{2,-8}{3,-7}{4,-11}{5,-9}", i + 1, "C", c.WardNo, c.BedNo, c.DailyRate, c.Available);
                }
            }
        }
        static void AddBed(List<Bed> bList)
        {
            //method for adding a bed to bed list
            bool flag = true;
            while(flag)
            {
                Console.Write("Enter Ward Type [A/B/C]: "); string wardtype = Console.ReadLine();
                Console.Write("Enter Ward No: "); string wardno = Console.ReadLine();
                Console.Write("Enter Bed No: "); string bedno = Console.ReadLine();
                Console.Write("Enter Daily Rate: $"); string dailyrate = Console.ReadLine();
                Console.Write("Enter Available [Y/N]: "); string available = Console.ReadLine();
                if (wardtype.ToUpper() == "A")
                {
                    //mostly validation checks
                    if(DigitString(wardno))
                    {
                        if(DigitString(bedno))
                        {
                            if(DigitString(dailyrate))
                            {
                                if(available.ToUpper() == "Y" || available.ToUpper() == "N")
                                {
                                    //adding bed to list
                                    Bed b = new ClassABed(Convert.ToInt32(wardno), Convert.ToInt32(bedno), Convert.ToDouble(dailyrate), BoolConverter(available));
                                    bList.Add(b);
                                    Console.WriteLine("Bed Added Successfully");
                                    string text = "A" + "," + wardno + "," + bedno + "," + StringConverter(BoolConverter(available)) + "," + dailyrate;
                                    using (StreamWriter file = new StreamWriter("Beds.csv", true))
                                    {
                                        file.WriteLine(text);
                                    }
                                    flag = false;
                                }
                                else
                                {
                                    Console.WriteLine("Invalid input! Please enter either Y or N");
                                    flag = false;
                                }
                            }
                            else
                            {
                                Console.WriteLine("Invalid input! Please enter numbers");
                                flag = false;
                            }
                        }
                        else
                        {
                            Console.WriteLine("Invalid input! Please enter a number");
                            flag = false;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Invalid input! Please enter a number");
                        flag = false;
                    }
                }
                else if (wardtype.ToUpper() == "B")
                {
                    //validation checks
                    if (DigitString(wardno))
                    {
                        if (DigitString(bedno))
                        {
                            if (DigitString(dailyrate))
                            {
                                if (available.ToUpper() == "Y" || available.ToUpper() == "N")
                                {
                                    //adding bed to list
                                    Bed b = new ClassBBed(Convert.ToInt32(wardno), Convert.ToInt32(bedno), Convert.ToDouble(dailyrate), BoolConverter(available));
                                    bList.Add(b);
                                    Console.WriteLine("Bed Added Successfully");
                                    string text = "A" + "," + wardno + "," + bedno + "," + StringConverter(BoolConverter(available)) + "," + dailyrate;
                                    using (StreamWriter file = new StreamWriter("Beds.csv", true))
                                    {
                                        file.WriteLine(text);
                                    }
                                    flag = false;
                                }
                                else
                                {
                                    Console.WriteLine("Invalid input! Please enter either Y or N");
                                    flag = false;
                                }
                            }
                            else
                            {
                                Console.WriteLine("Invalid input! Please enter numbers");
                                flag = false;
                            }
                        }
                        else
                        {
                            Console.WriteLine("Invalid input! Please enter a number");
                            flag = false;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Invalid input! Please enter a number");
                        flag = false;
                    }
                }
                else if (wardtype.ToUpper() == "C")
                {
                    //validation checks
                    if (DigitString(wardno))
                    {
                        if (DigitString(bedno))
                        {
                            if (DigitString(dailyrate))
                            {
                                if (available.ToUpper() == "Y" || available.ToUpper() == "N")
                                {
                                    //adding bed to list
                                    Bed b = new ClassCBed(Convert.ToInt32(wardno), Convert.ToInt32(bedno), Convert.ToDouble(dailyrate), BoolConverter(available));
                                    bList.Add(b);
                                    Console.WriteLine("Bed Added Successfully");
                                    string text = "A" + "," + wardno + "," + bedno + "," + StringConverter(BoolConverter(available)) + "," + dailyrate;
                                    using (StreamWriter file = new StreamWriter("Beds.csv", true))
                                    {
                                        file.WriteLine(text);
                                    }
                                    flag = false;
                                }
                                else
                                {
                                    Console.WriteLine("Invalid input! Please enter either Y or N");
                                    flag = false;
                                }
                            }
                            else
                            {
                                Console.WriteLine("Invalid input! Please enter numbers");
                                flag = false;
                            }
                        }
                        else
                        {
                            Console.WriteLine("Invalid input! Please enter a number");
                            flag = false;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Invalid input! Please enter a number");
                        flag = false;
                    }
                }
                else
                {
                    Console.WriteLine("Invalid Ward type. Please choose between A/B/C");
                    flag = false;
                }
            }

        }
        static void AddPatient(List<Patient> pList)
        {
            bool flag = true;
            while (flag)
            {
                Console.WriteLine();
                Console.Write("Enter Name: "); string name = Console.ReadLine();
                Console.Write("Enter Identification Number: "); string id = Console.ReadLine();
                Console.Write("Enter Age: "); string age = Console.ReadLine();
                Console.Write("Enter Gender [M/F]: "); string gen = Console.ReadLine();
                Console.Write("Enter Citizenship Status [SC/PR/Foreigner]: "); string cs = Console.ReadLine();
                Console.Write("Enter Balance (Not Applicable to Foreigners: "); string bal = Console.ReadLine();
                //validation checks
                if (DigitString(age))
                {
                    if (AlphaString(gen) && (gen.ToUpper() == "M" || gen.ToUpper() == "F"))
                    {
                        if (Convert.ToInt32(age) <= 12)
                        {//check if patient is child
                            if (AlphaString(name) && AlphaString(cs) && DigitString(bal))
                            {
                                if (cs.ToUpper() == "SC")
                                {
                                    Patient p = new Child(id, name, Convert.ToInt32(age), Convert.ToChar(gen), cs, "Registered", Convert.ToDouble(bal));
                                    Console.WriteLine("{0} is registered successfully", name);
                                    pList.Add(p);
                                    string text = name + "," + id + "," + age + "," + gen + "," + cs + "," + bal;
                                    using (StreamWriter file = new StreamWriter("Patients.csv", true))
                                    {
                                        file.WriteLine(text);
                                    }
                                    flag = false;
                                }
                                else if (cs.ToUpper() == "PR" || cs == "Foreigner")
                                {
                                    Patient p = new Child(id, name, Convert.ToInt32(age), Convert.ToChar(gen), cs, "Registered", 0);
                                    Console.WriteLine("{0} is registered successfully", name);
                                    pList.Add(p);
                                    string text = name + "," + id + "," + age + "," + gen + "," + cs;
                                    using (StreamWriter file = new StreamWriter("Patients.csv", true))
                                    {
                                        file.WriteLine(text);
                                    }
                                    flag = false;
                                }
                                else
                                {
                                    Console.WriteLine("Invalid Input Detected! Please Enter Valid Input...");
                                    flag = false;
                                }
                            }
                            else
                            {
                                Console.WriteLine("Invalid Input Detected! Please Enter Valid Input...");
                                flag = false;
                            }
                        }
                        else if (Convert.ToInt32(age) < 65)
                        {//check if patient is adult
                            if (AlphaString(name) && ValidID(id) && AlphaString(gen) && AlphaString(cs) && DigitString(bal))
                            {
                                if (cs.ToUpper() == "SC" || cs.ToUpper() == "PR")
                                {
                                    Patient p = new Adult(id, name, Convert.ToInt32(age), Convert.ToChar(gen), cs, "Registered", Convert.ToDouble(bal));
                                    Console.WriteLine("{0} is registered successfully", name);
                                    pList.Add(p);
                                    string text = name + "," + id + "," + age + "," + gen + "," + cs + "," + bal;
                                    using (StreamWriter file = new StreamWriter("Patients.csv", true))
                                    {
                                        file.WriteLine(text);
                                    }
                                    flag = false;
                                }
                                else if (cs == "Foreigner")
                                {
                                    Patient p = new Adult(id, name, Convert.ToInt32(age), Convert.ToChar(gen), cs, "Registered", 0);
                                    Console.WriteLine("{0} is registered successfully", name);
                                    pList.Add(p);
                                    string text = name + "," + id + "," + age + "," + gen + "," + cs;
                                    using (StreamWriter file = new StreamWriter("Patients.csv", true))
                                    {
                                        file.WriteLine(text);
                                    }
                                    flag = false;
                                }
                                else
                                {
                                    Console.WriteLine("Invalid Input Detected! Please Enter Valid Input...");
                                    flag = false;
                                }
                            }
                            else
                            {
                                Console.WriteLine("Invalid Input Detected! Please Enter Valid Input...");
                                flag = false;
                            }
                        }
                        else
                        {
                            if (AlphaString(name) && ValidID(id) && AlphaString(gen) && AlphaString(cs) && DigitString(bal))
                            {
                                Patient p = new Senior(id, name, Convert.ToInt32(age), Convert.ToChar(gen), cs, "Registered");
                                Console.WriteLine("{0} is registered successfully", name);
                                pList.Add(p);
                                string text = name + "," + id + "," + age + "," + gen + "," + cs;
                                using (StreamWriter file = new StreamWriter("Patients.csv", true))
                                {
                                    file.WriteLine(text);
                                }
                                flag = false;
                            }
                            else
                            {
                                Console.WriteLine("Invalid Input Detected! Please Enter Valid Input...");
                                flag = false;
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("Invalid Input. Please Enter Valid Input...");
                        flag = false;
                    }
                }
                else
                {
                    Console.WriteLine("Invalid Input. Please Enter Valid Input...");
                    flag = false;
                }
            }   
        }    
        static void RegisterStay(List<Patient> pList, List<Bed> bList)
        {
            bool flag = true;
            while (flag)
            {
                Console.WriteLine("{0,-13}{1,-10}{2,-5}{3,-7}{4,-13}{5,-12}{6,-8}", "Name", "ID", "Age", "Gender", "Citizenship", "Status", "Balance");
                foreach (Patient patient in pList)
                {
                    if (patient.Status == "Registered" || patient.Status == "Discharged")
                    {
                        if (patient is Child)
                        {
                            Child c = (Child)patient;
                            Console.WriteLine("{0,-13}{1,-10}{2,-5}{3,-7}{4,-13}{5,-12}{6,-8}", c.Name, c.ID, c.Age, c.Gender, c.CitizenStatus, c.Status, c.CdaBalance);
                        }
                        else if (patient is Adult)
                        {
                            Adult a = (Adult)patient;
                            Console.WriteLine("{0,-13}{1,-10}{2,-5}{3,-7}{4,-13}{5,-12}{6,-8}", a.Name, a.ID, a.Age, a.Gender, a.CitizenStatus, a.Status, a.MedisaveBalance);
                        }
                        else
                        {
                            Senior se = (Senior)patient;
                            Console.WriteLine("{0,-13}{1,-10}{2,-5}{3,-7}{4,-13}{5,-12}", se.Name, se.ID, se.Age, se.Gender, se.CitizenStatus, se.Status);
                        }
                    }
                }
                Patient p = RetrievePatient(pList);
                if (p != null)
                {
                    if (p.Stay == null)
                    {
                        DisplayBed(bList);
                        Console.Write("Enter number corresponded to bed number: "); string no = Console.ReadLine();
                        if (DigitString(no))
                        {
                            int bedno = Convert.ToInt32(no) - 1;
                            if (bedno <= bList.Count - 1)
                            {
                                Bed b = bList[bedno];
                                if (b.Available == true)
                                {
                                    Console.Write("Enter Date of Admission[MM/DD/YYYY]: "); string date = Console.ReadLine();
                                    if (DateValidation(date))
                                    {
                                        if (b is ClassABed)
                                        {
                                            Console.Write("Include Accompanying Person? [Y/N]: "); string check = Console.ReadLine().ToUpper();
                                            if (AlphaString(check) && (check == "Y" || check == "N"))
                                            {
                                                Bed newBed = new ClassABed(b.WardNo, b.BedNo, b.DailyRate, false);
                                                ClassABed nBed = (ClassABed)newBed;
                                                nBed.AccompanyingPerson = BoolConverter(check);
                                                b.Available = false;
                                                Stay s = new Stay(Convert.ToDateTime(date), p);
                                                BedStay bs = new BedStay(Convert.ToDateTime(date), newBed);
                                                s.DischargeDate = null;
                                                bs.EndBedstay = null;
                                                s.AddBedstay(bs);
                                                p.Stay = s;
                                                p.Status = "Admitted";
                                                p.Stay.IsPaid = false;
                                                Console.WriteLine("Hospital Stay Registration Successful.");
                                                flag = false;
                                            }
                                            else
                                            {
                                                Console.WriteLine("Invalid Input Detected! Please Enter Valid Input...");
                                                flag = false;
                                            }
                                        }
                                        else if (b is ClassBBed)
                                        {
                                            Console.Write("Include Air Conditioner? [Y/N]: "); string check = Console.ReadLine().ToUpper();
                                            if (AlphaString(check) && (check == "Y" || check == "N"))
                                            {
                                                Bed newBed = new ClassBBed(b.WardNo, b.BedNo, b.DailyRate, false);
                                                ClassBBed nBed = (ClassBBed)newBed;
                                                nBed.AirCon = BoolConverter(check);
                                                b.Available = false;
                                                Stay s = new Stay(Convert.ToDateTime(date), p);
                                                BedStay bs = new BedStay(Convert.ToDateTime(date), newBed);
                                                s.DischargeDate = null;
                                                bs.EndBedstay = null;
                                                s.AddBedstay(bs);
                                                p.Stay = s;
                                                p.Status = "Admitted";
                                                p.Stay.IsPaid = false;
                                                Console.WriteLine("Hospital Stay Registration Successful.");
                                                flag = false;
                                            }
                                            else
                                            {
                                                Console.WriteLine("Invalid Input Detected! Please Enter Valid Input...");
                                                flag = false;
                                            }
                                        }
                                        else
                                        {
                                            Console.Write("Include Portable Television? [Y/N]: "); string check = Console.ReadLine().ToUpper();
                                            if (AlphaString(check) && (check == "Y" || check == "N"))
                                            {
                                                Bed newBed = new ClassCBed(b.WardNo, b.BedNo, b.DailyRate, false);
                                                ClassCBed nBed = (ClassCBed)newBed;
                                                nBed.PortableTV = BoolConverter(check);
                                                b.Available = false;
                                                Stay s = new Stay(Convert.ToDateTime(date), p);
                                                BedStay bs = new BedStay(Convert.ToDateTime(date), newBed);
                                                s.DischargeDate = null;
                                                bs.EndBedstay = null;
                                                s.AddBedstay(bs);
                                                p.Stay = s;
                                                p.Status = "Admitted";
                                                p.Stay.IsPaid = false;
                                                Console.WriteLine("Hospital Stay Registration Successful.");
                                                flag = false;
                                            }
                                            else
                                            {
                                                Console.WriteLine("Invalid Input Detected! Please Enter Valid Input...");
                                                flag = false;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("Invalid Input Detected! Please Enter Valid Input...");
                                        flag = false;
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Bed Not Available. Please choose a different bed...");
                                    flag = false;
                                }
                            }
                            else
                            {
                                Console.WriteLine("Bed Not Found! Please Enter Valid Bed Number...");
                                flag = false;
                            }

                        }
                        else
                        {
                            Console.WriteLine("Invalid Input Detected! Please Enter Valid Input...");
                            flag = false;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Stay Already Registered! Please Choose Different Patient...");
                        flag = false;
                    }
                }
                else
                {
                    Console.WriteLine("Patient Not Found. Please Enter Valid Patient ID...");
                    flag = false;
                }
            }
        }
        static void PatientDetail(List<Patient> pList)
        {
            bool flag = true;
            while (flag)
            {
                DisplayPatient(pList);
                Patient p = RetrievePatient(pList);
                if (p != null)
                {
                    Console.WriteLine("======");
                    Console.WriteLine("Name: {0}", p.Name);
                    Console.WriteLine("Id: {0}", p.ID);
                    Console.WriteLine("Gender: {0}", p.Gender);
                    Console.WriteLine("Citizenship: {0}", p.CitizenStatus);
                    Console.WriteLine("Status: {0}", p.Status);
                    Console.WriteLine("======");
                    if (p.Stay != null)
                    {
                        Console.WriteLine("Date of Admission: {0}", p.Stay.AdmittedDate.ToString("MM/dd/yyyy"));
                        Console.WriteLine("Date of Discharge: {0}", p.Stay.DischargeDate.ToString());
                        Console.WriteLine("Payment Status: {0}", PaidConverter(p.Stay.IsPaid));
                        Console.WriteLine("======");
                        foreach (BedStay bs in p.Stay.BedStayList)
                        {
                            if (bs.Bed is ClassABed)
                            {
                                ClassABed db = (ClassABed)bs.Bed;
                                Console.WriteLine("Ward No.: {0}", db.WardNo);
                                Console.WriteLine("Bed No: {0}", db.BedNo);
                                Console.WriteLine("Bed Type: A");
                                Console.WriteLine("Start Bed Stay: {0}", bs.StartBedstay.ToString("MM/dd/yyyy"));
                                Console.WriteLine("End Bed Stay: {0}", bs.EndBedstay.ToString());
                                Console.WriteLine("Accompanying Person: {0}", db.AccompanyingPerson);
                                Console.WriteLine("======");
                            }
                            else if (bs.Bed is ClassBBed)
                            {
                                ClassBBed db = (ClassBBed)bs.Bed;
                                Console.WriteLine("Ward No.: {0}", db.WardNo);
                                Console.WriteLine("Bed No.: {0}", db.BedNo);
                                Console.WriteLine("Bed Type: A");
                                Console.WriteLine("Start Bed Stay: {0}", bs.StartBedstay.ToString("MM/dd/yyyy"));
                                Console.WriteLine("End Bed Stay: {0}", bs.EndBedstay.ToString());
                                Console.WriteLine("Air Conditioner: {0}", db.AirCon);
                                Console.WriteLine("======");
                            }
                            else
                            {
                                ClassCBed db = (ClassCBed)bs.Bed;
                                Console.WriteLine("Ward No.: {0}", db.WardNo);
                                Console.WriteLine("Bed No.: {0}", db.BedNo);
                                Console.WriteLine("Bed Type: A");
                                Console.WriteLine("Start Bed Stay: {0}", bs.StartBedstay.ToString("MM/dd/yyyy"));
                                Console.WriteLine("End Bed Stay: {0}", bs.EndBedstay.ToString());
                                Console.WriteLine("Portable TV: {0}", db.PortableTV);
                                Console.WriteLine("======");
                            }
                        }
                        flag = false;
                    }
                    else
                    {
                        Console.WriteLine("Stay not registered under selected patient.");
                        flag = false;
                    }
                }
                else
                {
                    Console.WriteLine("Patient Not Found! Please Enter Valid Patient ID...");
                    flag = false;
                }
            }
        }

        static void AddMedicalRecord(List<Patient> pList)
        {
            //method for adding a medical record to a patient
            bool flag = true;
            while (flag)
            {
                //displaying list of patients and letting user choose patientid 
                DisplayPatient(pList);
                Patient p = RetrievePatient(pList);
                if(p != null)
                {
                    if (p.Stay != null)
                    {
                        Console.Write("Patient Temperature: "); string temperature = Console.ReadLine();
                        Console.Write("Please Enter patient observation: "); string diagnosis = Console.ReadLine();
                        Console.Write("Enter Date Entered: "); string dte = Console.ReadLine();
                        //validation checks
                        if (DigitString(temperature))
                        {
                            if (DateValidation(dte))
                            {
                                MedicalRecord med = new MedicalRecord(diagnosis, Convert.ToDouble(temperature), Convert.ToDateTime(dte));
                                p.Stay.AddMedicalRecord(med);
                                Console.WriteLine("Medical record entry for {0} successful.", p.Name);
                                flag = false;
                            }
                            else
                            {
                                Console.WriteLine("Invalid input! Please input a date");
                                flag = false;
                            }
                        }
                        else
                        {
                            Console.WriteLine("Invalid input! Please input a temperature");
                            flag = false;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Stay not Registered under patient!");
                        flag = false;
                    }
                }
                else
                {
                    Console.WriteLine("Entry Unsuccessful.");
                    flag = false;
                }
            }
        }
        static void ViewRecords(List<Patient> pList)
        {
            bool flag = true;
            while (flag)
            {
                DisplayPatient(pList);
                Patient p = RetrievePatient(pList);
                if(p != null)
                {
                    if ((p.Stay != null) && (p.Stay.MedicalRecordList != null))
                    {
                        foreach(MedicalRecord med in p.Stay.MedicalRecordList)
                        {
                            Console.WriteLine("Name of Patient: {0}", p.Name);
                            Console.WriteLine("ID Number: {0}", p.ID);
                            Console.WriteLine("Citizenship Status: {0}", p.CitizenStatus);
                            Console.WriteLine("Gender: {0}", p.Gender);
                            Console.WriteLine("Status: {0}", p.Status);

                            Console.WriteLine("======Stay======");
                            Console.WriteLine("Admission Date: " + p.Stay.AdmittedDate + "\n"
                                                + "Discharge Date: " + p.Stay.DischargeDate);

                            Console.WriteLine(med);
                            flag = false;
                        }
                    }
                    else if (p.Stay == null)
                    {
                        Console.WriteLine("Please register stay to check medical record.");
                        flag = false;
                    }
                    else if (p.Stay.MedicalRecordList == null)
                    {
                        Console.WriteLine("No medical record.");
                        flag = false;
                    }
                    else
                    {
                        Console.WriteLine("Invalid input. Please enter a patient's ID");
                        flag = false;
                    }
                }
            }
        }
        static void ChangeBed(List<Patient> pList, List<Bed> bList)
        {
            bool flag = true;
            while (flag)
            {
                Console.WriteLine("{0,-13}{1,-10}{2,-5}{3,-7}{4,-13}{5,-12}{6,-8}", "Name", "ID", "Age", "Gender", "Citizenship", "Status", "Balance");
                foreach (Patient patient in pList)
                {
                    if (patient.Status == "Admitted")
                    {
                        if (patient is Child)
                        {
                            Child c = (Child)patient;
                            Console.WriteLine("{0,-13}{1,-10}{2,-5}{3,-7}{4,-13}{5,-12}{6,-8}", c.Name, c.ID, c.Age, c.Gender, c.CitizenStatus, c.Status, c.CdaBalance);
                        }
                        else if (patient is Adult)
                        {
                            Adult a = (Adult)patient;
                            Console.WriteLine("{0,-13}{1,-10}{2,-5}{3,-7}{4,-13}{5,-12}{6,-8}", a.Name, a.ID, a.Age, a.Gender, a.CitizenStatus, a.Status, a.MedisaveBalance);
                        }
                        else
                        {
                            Senior se = (Senior)patient;
                            Console.WriteLine("{0,-13}{1,-10}{2,-5}{3,-7}{4,-13}{5,-12}", se.Name, se.ID, se.Age, se.Gender, se.CitizenStatus, se.Status);
                        }
                    }
                }
                Patient p = RetrievePatient(pList);
                if (p != null && p.Status == "Admitted")
                {
                    Stay s = p.Stay;
                    DisplayBed(bList);
                    Console.Write("Enter bed number: "); string no = Console.ReadLine();
                    int bedno = Convert.ToInt32(no) - 1;
                    if (bedno <= bList.Count - 1)
                    {
                        Bed b = bList[bedno];
                        if (b.Available == true)
                        {
                            Console.Write("Enter Date of transfer[MM/DD/YY]: "); string transferDate = Console.ReadLine();
                            if (DateValidation(transferDate) && OrderDate(p.Stay.BedStayList[p.Stay.BedStayList.Count - 1].StartBedstay, Convert.ToDateTime(transferDate)))
                            {
                                if (b is ClassABed)
                                {
                                    Console.Write("Include Accompanying Person? [Y/N]: "); string check = Console.ReadLine().ToUpper();
                                    if (AlphaString(check) && (check == "Y" || check == "N"))
                                    {
                                        Bed newBed = new ClassABed(b.WardNo, b.BedNo, b.DailyRate, false);
                                        ClassABed nBed = (ClassABed)newBed;
                                        nBed.AccompanyingPerson = BoolConverter(check);
                                        b.Available = false;
                                        s.BedStayList[(s.BedStayList.Count - 1)].EndBedstay = Convert.ToDateTime(transferDate);
                                        foreach (Bed bed in bList)
                                        {
                                            if ((bed.BedNo == s.BedStayList[(s.BedStayList.Count - 1)].Bed.BedNo) && (bed.WardNo == s.BedStayList[(s.BedStayList.Count - 1)].Bed.WardNo))
                                            {
                                                bed.Available = true;
                                            }
                                        }
                                        BedStay bs = new BedStay(Convert.ToDateTime(transferDate), newBed);
                                        s.AddBedstay(bs);
                                        Console.WriteLine("Bed Transfer Successful");
                                        flag = false;
                                    }
                                    else
                                    {
                                        Console.WriteLine("Invalid Input Detected! Please Enter Valid Input...");
                                        flag = false;
                                    }
                                }
                                else if (b is ClassBBed)
                                {
                                    Console.Write("Include Air Conditioner? [Y/N]: "); string check = Console.ReadLine().ToUpper();
                                    if (AlphaString(check) && (check == "Y" || check == "N"))
                                    {
                                        Bed newBed = new ClassBBed(b.WardNo, b.BedNo, b.DailyRate, false);
                                        ClassBBed nBed = (ClassBBed)newBed;
                                        nBed.AirCon = BoolConverter(check);
                                        b.Available = false;
                                        s.BedStayList[(s.BedStayList.Count - 1)].EndBedstay = Convert.ToDateTime(transferDate);
                                        foreach (Bed bed in bList)
                                        {
                                            if ((bed.BedNo == s.BedStayList[(s.BedStayList.Count - 1)].Bed.BedNo) && (bed.WardNo == s.BedStayList[(s.BedStayList.Count - 1)].Bed.WardNo))
                                            {
                                                bed.Available = true;
                                            }
                                        }
                                        BedStay bs = new BedStay(Convert.ToDateTime(transferDate), newBed);
                                        s.AddBedstay(bs);
                                        Console.WriteLine("Bed Transfer Successful");
                                        flag = false;
                                    }
                                    else
                                    {
                                        Console.WriteLine("Invalid Input Detected! Please Enter Valid Input...");
                                        flag = false;
                                    }
                                }
                                else
                                {
                                    Console.Write("Include Portable TV? [Y/N]: "); string check = Console.ReadLine().ToUpper();
                                    if (AlphaString(check) && (check == "Y" || check == "N"))
                                    {
                                        Bed newBed = new ClassCBed(b.WardNo, b.BedNo, b.DailyRate, false);
                                        ClassCBed nBed = (ClassCBed)newBed;
                                        nBed.PortableTV = BoolConverter(check);
                                        b.Available = false;
                                        s.BedStayList[(s.BedStayList.Count - 1)].EndBedstay = Convert.ToDateTime(transferDate);
                                        foreach (Bed bed in bList)
                                        {
                                            if ((bed.BedNo == s.BedStayList[(s.BedStayList.Count - 1)].Bed.BedNo) && (bed.WardNo == s.BedStayList[(s.BedStayList.Count - 1)].Bed.WardNo))
                                            {
                                                bed.Available = true;
                                            }
                                        }
                                        BedStay bs = new BedStay(Convert.ToDateTime(transferDate), newBed);
                                        s.AddBedstay(bs);
                                        Console.WriteLine("Bed Transfer Successful");
                                        flag = false;
                                    }
                                    else
                                    {
                                        Console.WriteLine("Invalid Input Detected! Please Enter Valid Input...");
                                        flag = false;
                                    }
                                }
                            }
                            else
                            {
                                Console.WriteLine("Invalid Input Detected! Please Enter Valid Input...");
                                flag = false;
                            }
                        }
                        else
                        {
                            Console.WriteLine("Bed Not Available! Please Choose a Different Bed...");
                            flag = false;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Invalid Input Detected! Please Enter Valid Input...");
                        flag = false;
                    }
                }
                else
                {
                    Console.WriteLine("Patient Not Found! Please Enter Valid Patient ID...");
                    flag = false;
                }
            }
        }
        static void DischargePayment(List<Patient> pList, List<Bed> bList)
        {
            bool flag = true;
            while (flag)
            {
                Console.WriteLine("{0,-13}{1,-10}{2,-5}{3,-7}{4,-13}{5,-12}{6,-8}", "Name", "ID", "Age", "Gender", "Citizenship", "Status", "Balance");
                foreach (Patient patient in pList)
                {
                    if (patient.Status == "Admitted")
                    {
                        if (patient is Child)
                        {
                            Child c = (Child)patient;
                            Console.WriteLine("{0,-13}{1,-10}{2,-5}{3,-7}{4,-13}{5,-12}{6,-8}", c.Name, c.ID, c.Age, c.Gender, c.CitizenStatus, c.Status, c.CdaBalance);
                        }
                        else if (patient is Adult)
                        {
                            Adult a = (Adult)patient;
                            Console.WriteLine("{0,-13}{1,-10}{2,-5}{3,-7}{4,-13}{5,-12}{6,-8}", a.Name, a.ID, a.Age, a.Gender, a.CitizenStatus, a.Status, a.MedisaveBalance);
                        }
                        else
                        {
                            Senior se = (Senior)patient;
                            Console.WriteLine("{0,-13}{1,-10}{2,-5}{3,-7}{4,-13}{5,-12}", se.Name, se.ID, se.Age, se.Gender, se.CitizenStatus, se.Status);
                        }
                    }
                }
                Patient p = RetrievePatient(pList);
                if (p != null && p.Status == "Admitted")
                {
                    Console.Write("Date of Discharge: "); string Dis = Console.ReadLine();
                    if(DateValidation(Dis))
                    {
                        DateTime Discharge = Convert.ToDateTime(Dis);
                        if(OrderDate(p.Stay.AdmittedDate, Discharge))
                        {
                            p.Status = "Discharged";
                            foreach (Bed bed in bList)
                            {
                                if ((bed.BedNo == p.Stay.BedStayList[(p.Stay.BedStayList.Count - 1)].Bed.BedNo) && (bed.WardNo == p.Stay.BedStayList[(p.Stay.BedStayList.Count - 1)].Bed.WardNo))
                                {
                                    bed.Available = true;
                                }
                            }
                            p.Stay.DischargeDate = Discharge;
                            p.Stay.BedStayList[p.Stay.BedStayList.Count - 1].EndBedstay = Discharge;
                            Console.WriteLine("Name of Patient: " + p.Name + "\n" +
                                "ID Number: " + p.ID + "\n" +
                                "Citizenship Status: " + p.CitizenStatus + "\n" +
                                "Gender: " + p.Gender + "\n" +
                                "Status: " + p.Status);

                            Console.WriteLine("======Stay======");
                            Console.WriteLine("Admission Date: " + p.Stay.AdmittedDate + "\n" +
                                "Discharged Date: " + p.Stay.DischargeDate + "\n" +
                                "Payment Status: " + p.Stay.IsPaid);
                            for (int i = 0; i < p.Stay.BedStayList.Count; i++)
                            {
                                BedStay bs = p.Stay.BedStayList[i];
                                Console.WriteLine("======Bed #{0}=======", i + 1);
                                if (bs.Bed is ClassABed)
                                {
                                    ClassABed downcast = (ClassABed)bs.Bed;
                                    Console.WriteLine("Ward Number: " + downcast.WardNo + "\n"
                                        + "Bed Number: " + downcast.BedNo + "\n"
                                        + "Ward Class: A" + "\n"
                                        + "Start of Bed Stay: " + bs.StartBedstay + "\n"
                                        + "End of Bed Stay: " + bs.EndBedstay + "\n"
                                        + "Accompanying Person: " + downcast.AccompanyingPerson);
                                }
                                else if (bs.Bed is ClassBBed)
                                {
                                    ClassBBed downcast = (ClassBBed)bs.Bed;
                                    Console.WriteLine("Ward Number: " + downcast.WardNo + "\n"
                                        + "Bed Number: " + downcast.BedNo + "\n"
                                        + "Ward Class: B" + "\n"
                                        + "Start of Bed Stay: " + bs.StartBedstay + "\n"
                                        + "End of Bed Stay: " + bs.EndBedstay + "\n"
                                        + "Aircon: " + downcast.AirCon);
                                }
                                else if (bs.Bed is ClassCBed)
                                {
                                    ClassCBed downcast = (ClassCBed)bs.Bed;
                                    Console.WriteLine("Ward Number: " + downcast.WardNo + "\n"
                                        + "Bed Number: " + downcast.BedNo + "\n"
                                        + "Ward Class: A" + "\n"
                                        + "Start of Bed Stay: " + bs.StartBedstay + "\n"
                                        + "End of Bed Stay: " + bs.EndBedstay + "\n"
                                        + "Portable TV: " + downcast.PortableTV);
                                }
                            }
                            TimeSpan staylength = p.Stay.DischargeDate.Value - p.Stay.AdmittedDate;
                            int differenceInDays = staylength.Days;
                            Console.WriteLine("Number of Days stayed: {0}", differenceInDays + "\n"
                                                + "==========");
                            if (p.Stay.IsPaid == false)
                            {
                                if (p is Child)
                                {
                                    if (p.CitizenStatus is "SC")
                                    {
                                        Child downcast = (Child)p;
                                        Console.WriteLine("Outstanding Charges: {0}", downcast.CalculateCharges());
                                        Console.WriteLine("CDA Balance: {0}", downcast.CdaBalance);
                                        if (downcast.CdaBalance >= downcast.CalculateCharges())
                                        {
                                            Console.WriteLine("To Deduct from CDA: ${0}", p.CalculateCharges());
                                            Console.WriteLine("[Press any key to proceed with payment]");
                                            Console.ReadKey();

                                            Console.WriteLine("Commencing payment...");
                                            downcast.CdaBalance = downcast.CdaBalance - downcast.CalculateCharges();

                                            Console.WriteLine("${0} has been deducted from CDA", downcast.CalculateCharges());
                                            Console.WriteLine("New CDA Balance: ${0}", downcast.CdaBalance);
                                            Console.WriteLine("Payment successful!");
                                            flag = false;
                                        }
                                        else
                                        {
                                            Console.WriteLine("To deduct from CDA: ${0}", downcast.CdaBalance);
                                            double subtotal = downcast.CalculateCharges() - downcast.CdaBalance;
                                            Console.WriteLine("Sub-total: ${0}", subtotal);
                                            Console.WriteLine("[Press any key to proceed with payment]");
                                            Console.ReadKey();
                                            Console.WriteLine();
                                            Console.WriteLine("Commencing payment...");
                                            Console.WriteLine();
                                            Console.WriteLine("${0} has been deducted from CDA", downcast.CdaBalance);
                                            downcast.CdaBalance = 0;
                                            Console.WriteLine("New CDA Balance: ${0}", downcast.CdaBalance);
                                            Console.WriteLine("Sub-total: ${0} has been paid by cash", subtotal);
                                            Console.WriteLine();
                                            Console.WriteLine("Payment Succcessful");
                                            flag = false;
                                        }
                                    }
                                    else
                                    {
                                        Nonsubsidsedpayment(p);
                                        flag = false;
                                    }
                                }
                                else if (p is Adult)
                                {
                                    if (p.CitizenStatus is "SC" || p.CitizenStatus is "PR")
                                    {
                                        Adult downcast = (Adult)p;
                                        Console.WriteLine("Outstanding Charges: {0}", downcast.CalculateCharges());
                                        Console.WriteLine("CDA Balance: {0}", downcast.MedisaveBalance);
                                        if (downcast.MedisaveBalance >= downcast.CalculateCharges())
                                        {
                                            Console.WriteLine("To Deduct from CDA: ${0}", downcast.CalculateCharges());
                                            Console.WriteLine("[Press any key to proceed with payment]");
                                            Console.ReadKey();

                                            Console.WriteLine("Commencing payment...");
                                            downcast.MedisaveBalance = downcast.MedisaveBalance - downcast.CalculateCharges();

                                            Console.WriteLine("${0} has been deducted from CDA", downcast.CalculateCharges());
                                            Console.WriteLine("New CDA Balance: ${0}", downcast.MedisaveBalance);
                                            Console.WriteLine("Payment successful!");
                                            flag = false;
                                        }
                                        else
                                        {
                                            Console.WriteLine("To deduct from CDA: ${0}", downcast.MedisaveBalance);
                                            double subtotal = downcast.CalculateCharges() - downcast.MedisaveBalance;
                                            Console.WriteLine("Sub-total: ${0}", subtotal);
                                            Console.WriteLine("[Press any key to proceed with payment]");
                                            Console.ReadKey();
                                            Console.WriteLine();
                                            Console.WriteLine("Commencing payment...");
                                            Console.WriteLine();
                                            Console.WriteLine("${0} has been deducted from CDA", downcast.MedisaveBalance);
                                            downcast.MedisaveBalance = 0;
                                            Console.WriteLine("New CDA Balance: ${0}", downcast.MedisaveBalance);
                                            Console.WriteLine("Sub-total: ${0} has been paid by cash", subtotal);
                                            Console.WriteLine();
                                            Console.WriteLine("Payment Succcessful");
                                            flag = false;
                                        }
                                    }
                                    else
                                    {
                                        Nonsubsidsedpayment(p);
                                        flag = false;
                                    }
                                }
                                else
                                {
                                    Senior downcast = (Senior)p;
                                    Console.WriteLine("Total Charges: ${0}", downcast.CalculateCharges());
                                    Console.WriteLine("[Press any key to proceed with payment]");
                                    Console.ReadKey();

                                    Console.WriteLine("Commencing payment...");
                                    Console.WriteLine("${0} has been paid by cash", downcast.CalculateCharges());
                                    Console.WriteLine("Payment successful!");
                                    flag = false;
                                }
                            }
                        }                    
                    }
                    else
                    {
                        Console.WriteLine("Invalid input! Please enter a date");
                        flag = false;
                    }
                }
                else
                {
                    Console.WriteLine("Patient not found!");
                    flag = false;
                }
            }
        }
        static void APIPM()
        {
            
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://api.data.gov.sg");
                Task<HttpResponseMessage> responseTask = client.GetAsync("/v1/environment/pm25");
                responseTask.Wait();
                HttpResponseMessage result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    Task<string> readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();
                    string data = readTask.Result;
                    RootObject rootObject = JsonConvert.DeserializeObject<RootObject>(data);
                    Item item=rootObject.items[0];
                    Console.WriteLine("\n============================3.2 Display the PM 2.5 information=================================\n");
                    Console.WriteLine("Timestamp: " + item.timestamp+"\n");
                    Console.WriteLine("West: " + item.readings.pm25_one_hourly.west);
                    Console.WriteLine("East: " + item.readings.pm25_one_hourly.east);
                    Console.WriteLine("Central: " + item.readings.pm25_one_hourly.central);
                    Console.WriteLine("North: " + item.readings.pm25_one_hourly.north);
                    Console.WriteLine("South: " + item.readings.pm25_one_hourly.south);
                    
                }
            }
        }
        static void ExchangeRate()
        {
            ExRate rate = new ExRate();
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://api.exchangeratesapi.io");
                Task<HttpResponseMessage> responseTask = client.GetAsync("/latest?base=SGD");
                responseTask.Wait();
                HttpResponseMessage result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    Task<string> readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();

                    string data = readTask.Result;
                    rate = JsonConvert.DeserializeObject<ExRate>(data);
                    foreach (var index in rate.Rates)
                    {
                        Console.WriteLine($"{index.Key}: {index.Value}");
                    }
                }
            }
        }
        ////Miscellaneous Methods
        static bool BoolConverter(string b)
        {
            if (b == "Yes" || b == "Y")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        static string PaidConverter(bool p)
        {
            if (p == true)
            {
                return "Paid";
            }
            else
            {
                return "Unpaid";
            }
        }
        static Patient RetrievePatient(List<Patient> pList)
        {
            Patient patient = null;
            Console.Write("Enter Patient ID Number: "); string id = Console.ReadLine();
            foreach (Patient person in pList)
            {
                if (person.ID == id)
                {
                    patient = person;
                }
            }
            return patient;
        }
        static void Nonsubsidsedpayment(Patient p)
        {
            Console.WriteLine("Outstanding Charges: $" + p.CalculateCharges());
            Console.WriteLine("[Press any key to proceed with payment]");
            Console.ReadKey();

            Console.WriteLine("Commencing payment...");
            Console.WriteLine("${0} has been paid by cash", p.CalculateCharges());
            Console.WriteLine("Payment successful!");
        }
        static bool DateValidation(string d)
        {
            DateTime tempDate;
            if (DateTime.TryParse(d , out tempDate))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        static bool OrderDate(DateTime d1, DateTime d2)
        {
            if (d2 < d1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        static bool AlphaString(string s)
        {
            if (s.Any(char.IsDigit))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        static bool DigitString(string s)
        {
            if (s.Any(char.IsLetter))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        static bool ValidID(string s)
        {
            bool flag = false;
            if (s.Length == 9)
            {
                for (int i = 0; i < s.Length; i++)
                {
                    if (i == 0)
                    {
                        if (Char.IsLetter(s[i]))
                        {
                            flag = true;
                        }
                        else
                        {
                            flag = false;
                        }
                    }
                    else if (i == 8)
                    {
                        if (Char.IsLetter(s[i]))
                        {
                            flag = true;
                        }
                        else
                        {
                            flag = false;
                        }
                    }
                    else
                    {
                        if (Char.IsDigit(s[i]))
                        {
                            flag = true;
                        }
                        else
                        {
                            flag = false;
                        }
                    }
                }
            }
            else
            {
                flag = false;
            }
            return flag;
        }
        static string StringConverter(bool b)
        {
            if (b == true)
            {
                return "Yes";
            }
            else
            {
                return "No";
            }
        }
    }
}