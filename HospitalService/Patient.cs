﻿//============================================================
// Student Number	: S10194816, S10195629
// Student Name	: Isaiah Low Jung, Glenn Peh Chia Kit
// Module  Group	: T01 
//============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HospitalService
{
    class Patient
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public char Gender { get; set; }
        public string CitizenStatus { get; set; }
        public string Status { get; set; }
        public Stay Stay { get; set; }

        public Patient (string i, string n, int a, char g, string cs, string s)
        {
            ID = i;
            Name = n;
            Age = a;
            Gender = g;
            CitizenStatus = cs;
            Status = s;
        }
        public virtual double CalculateCharges()
        {
            double total = 0;
            foreach (BedStay bs in Stay.BedStayList)
            {
                if (bs.Bed != null)
                {
                    TimeSpan StayLength = bs.EndBedstay.Value - bs.StartBedstay;
                    int DifferenceInDays = StayLength.Days;
                    total = total + bs.Bed.CalculateCharges(CitizenStatus, DifferenceInDays);
                }
            }
            return total;
        }

        public override string ToString()
        {
            return "ID: " + ID +
                "\tName: " + Name +
                "\tAge: " + Age +
                "\tGender: " + Gender +
                "\tCitizenship: " + CitizenStatus +
                "\tStatus: " + Status +
                "\tStay: " + Stay;
        }
    }
}
